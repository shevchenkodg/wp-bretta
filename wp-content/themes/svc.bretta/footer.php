<div class="footer-reserved"></div>
</div>

<div class="footer">
	<footer>
		<div class="menu">
			<div class="wrapper">
				<div class="container-fluid p0">
					<nav class="widescreen-menu">
						<?php wp_nav_menu(['theme_location' => 'menu', 'container' => false, 'menu_class' => 'clearfix']); ?>
                    </nav>
				</div>
			</div>
		</div>
		<div class="contacts">
			<div class="wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-2 col-md-2 item">
                            <a href="/">
	                            <?php if(!dynamic_sidebar('footer_logo')): ?>Виджет "Логотип в футере"<?php endif; ?>
                            </a>
							<div>Интернет-магазин гранитных моек</div>
						</div>
						<div class="col-lg-3 col-md-3 item">
							<?php if(!dynamic_sidebar('footer_email')): ?>Виджет "Email в футере"<?php endif; ?>
						</div>
						<div class="col-lg-3 col-md-3 item">
							<?php if(!dynamic_sidebar('footer_phones')): ?>Виджет "Телефоны в футере"<?php endif; ?>
						</div>
						<div class="col-lg-3 col-md-3 item">
							<?php if(!dynamic_sidebar('footer_address')): ?>Виджет "Адрес в футере"<?php endif; ?>
                        </div>
						<div class="col-lg-1 col-md-1 item">
							<?php if(!dynamic_sidebar('footer_skype')): ?>Виджет "Skype в футере"<?php endif; ?>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright">&copy; Copyright 2017 BRETTA</div>
	</footer>
</div>
<div id="toTop"><i class="fa fa-angle-up" aria-hidden="true"></i></div>
<?php wp_footer(); ?>

<link rel="stylesheet"  href="<?php bloginfo('template_url'); ?>/assets/css/lightslider.min.css"/>

<script src="<?php bloginfo('template_url'); ?>/assets/js/lightslider.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.ajax-popup-link').magnificPopup({
            removalDelay: 500,
            tLoading: 'Загрузка...',
            type: 'ajax',
            callbacks: {
                beforeOpen: function() {
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
            }
        });
    });
    // Scroll-to-top button
    $(function() {
        $(window).scroll(function() {
            if($(this).scrollTop() != 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });
        $('#toTop').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });
</script>

<script>
    /* Open when someone clicks on the span element */
    function openNav() {
        document.getElementById("menuSmallScreen").style.width = "100%";
    }
    /* Close when someone clicks on the "x" symbol inside the overlay */
    function closeNav() {
        document.getElementById("menuSmallScreen").style.width = "0%";
    }
</script>

</body>
</html>