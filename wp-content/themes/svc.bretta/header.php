<!DOCTYPE html>
<html lang="ru">
<head>
	<title><?php bloginfo('name'); wp_title();?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1, user-scalable=no" />
	<?php wp_head(); ?>
</head>
<body>
<div class="main-wrapper">
	<header>
		<div class="wrapper">
			<div class="container-fluid">
				<div class="row hidden-sm hidden-xs">
					<div class="col-lg-2 col-md-2 mt15">
						<a href="/" class="logo">
							<?php if(!dynamic_sidebar('header_logo')): ?>Виджет "Логотип в шапке"<?php endif; ?>
                        </a>
					</div>
					<div class="col-lg-4 col-md-3 mt15">
						<form class="search" method="get" action="">
							<input type="text" name="s" placeholder="Поиск" value="<?php if($s = get_query_var( 's', 1 )) echo $s; ?>" required>
							<button><i class="fa fa-search" aria-hidden="true"></i></button>
						</form>
					</div>
					<div class="col-lg-4 col-md-4 mt15">
						<div class="phones">
								<?php if(!dynamic_sidebar('header_phone')): ?>
                                    Номер телефона не указан
								<?php endif; ?>
						</div>
						<div class="callback">
							<a href="/callback" class="ajax-popup-link" data-effect="mfp-move-from-top">Заказать звонок</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 acc-col">
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid one-level-menu">
			<!-- Menu for MD and LG -->
			<div class="menu-widescreen hidden-sm hidden-xs <?php if(is_front_page()): echo 'home'; endif;?>">
				<div class="wrapper">
					<div class="row w100ps m0">
						<div class="col-lg-12 p0">
							<nav>
								<?php wp_nav_menu(['theme_location' => 'menu', 'container' => false, 'menu_class' => 'clearfix']); ?>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<!-- Menu for SM and XS -->
			<div class="menu-smallscreen">
				<div class="wrapper hidden-md hidden-lg">
					<div class="row w100ps">
						<div class="col-lg-12 mt15 tcenter">
							<div class="menu-btn">
								<a href="#" onclick="openNav()" id="menuBtn"><i class="fa fa-bars" aria-hidden="true"></i></a>
							</div>
							<!-- The overlay -->
							<div id="menuSmallScreen" class="overlay">
								<div class="overlay-content">
									<div class="items">
										<div class="search">
											<form method="get" action="">
												<input type="text" name="s" placeholder="Поиск" value="<?php if($s = get_query_var( 's', 1 )) echo $s; ?>" required>
												<button><i class="fa fa-search" aria-hidden="true"></i></button>
											</form>
										</div>
										<nav>
											<ul>
												<?php wp_nav_menu(['theme_location' => 'menu', 'container' => false]); ?>
                                            </ul>
										</nav>
<!--										<div class="acc">-->
<!--											<a href="#"><i class="fa fa-user" aria-hidden="true"></i>Войти</a>-->
<!--										</div>-->
										<div class="phones">
											<a class="tel" href="tel:+380660304500"><i class="fa fa-mobile" aria-hidden="true"></i>+380 (66) 030-45-00</a>
										</div>
										<div class="callback">
											<a href="/callback" class="ajax-popup-link" data-effect="mfp-move-from-top">Заказать звонок</a>
										</div>
									</div>
									<div class="close-menu">
										<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
									</div>
								</div>
							</div>
							<a href="/">
								<?php if(!dynamic_sidebar('header_logo')): ?>Виджет "Логотип в хедере"<?php endif; ?>
                            </a>
							<div class="cart-wrapper">
<!--								<a href="cart.html" class="cart ajax-popup-cart-link" data-effect="mfp-move-from-top">-->
<!--									<i class="fa fa-shopping-cart" aria-hidden="true"></i>-->
<!--									<div class="count">2</div>-->
<!--								</a>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>