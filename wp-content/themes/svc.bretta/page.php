<?php
get_header();
require_once('partials/partial.breadcrumbs.php');
?>

<div class="wrapper">
	<div class="container-fluid content news">
		<div class="row">
			<div class="col-lg-12">
				<div class="article">
					<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
						<div class="title"><?php the_title() ?></div>
						<div class="content">
							<?php if(get_the_content()) {
								the_content();
							}
							else {
								?>
                                <div class="tcenter mt30">Контент этой страницы еще не размещен.</div>
								<?php
							}
							?>
						</div>
					<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>