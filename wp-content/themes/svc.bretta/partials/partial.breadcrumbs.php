<div class="breadcrumbs hidden-xs hidden-sm hidden-md">
	<div class="wrapper">
		<div class="container-fluid">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
		</div>
	</div>
</div>
<div class="breadcrumbs smallscreen hidden-lg">
	<div class="wrapper">
		<div class="container-fluid">
			<a href="#" onclick="history.go(-1);" class="back"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
			<span><?php the_title() ?></span>
		</div>
	</div>
</div>